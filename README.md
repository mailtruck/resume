Brian Danowski
briandanowski@gmail.com
313-518-2656
Ferndale, Michigan

SENIOR TECHNICAL TEAM LEAD

Software Development (Agile with Automation Skills)

Collaborative Software Development Professional with experience leveraging automation skills to provide high leverage value that truly scales teams. Facilitates communication between leadership and teams for continuous improvement via sprint retrospectives. Delivers features, improvements, and value via the sprint process and constantly evaluating the goals of the team vs assigned team work. 

Core Competencies

Automation  |  CI/CD Pipeline Development  |  Software Development  |  GIT  |  SCM  |  Kubernetes  |  Golang  | Software Testing  |  Microservices Architectures  |  Monolith Architectures  | Team Collaboration Across API Boundaries  |  API Integrations (third party)  |  API Management via gateway (Kong)  |  Bug hunting and fixing  |  Agile Management Processes  |  Collaboration and Onboarding for Teams | Full Stack Development via backend frontend and styling skills  |  Management and moving to the future of Lerna Powered Mono repo for React JavaScript Development  |  Legacy software development via a PHP Drupal Monolith

Professional Experience

Benzinga 
2015-2020

Software Devlopment, Team Leadership, Automation

Used skills developing CI/CD jobs for a number of Microservices to provide scalable value through automation. Reviewed code submitted by team members. Created high value Microservices integrations via Golang and Kubernetes. Ensured quality through software testing

• Establishment and adoption of Sprint Retrospectives to shorten feedback loop of team success and rapid improvement of company culture
• Unification of API Documentation via Swagger Specs creating framework for sharing documentation internally and externally to lower friction of development and integration against APIs
• Publication of API Documentation via Public Documentation site https://docs.benzinga.io increased communication with customers both current and potential. Used as a strong sales tool.
• Organization and management of API’s via Kong Gateway Allowed rapid integration with third party APIs via a consistent structure that helped ensure a high velocity for development and sales teams.
• Integration of third party data feeds and APIs allowed sales and development to have good collaboration for strong shared goals of increasing revenue, 
• Ensuring code quality via Software Testing practices allowed future development to smoothly onboard new features while ensuring that there are no (or few) regressions whenever possible
• Rapid development of features to deliver value for customers allowed sales and development teams to have harmonious relationship in the shared vision of increasing revenue.

Vinsetta Garage, Monterey Cantina, Woodward Ave Brewers 
2011-2015

Busser, Food Runner, Dish Washer

Leadership through being the change. Rapid response to customer needs to provide high quality service environment. Ensured a smooth feedback loop between team leadership and customers through communication